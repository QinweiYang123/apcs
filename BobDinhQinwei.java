/*
Finished version
does not work with drjava older version (2012) = eclipse complier 0.A48 since the class JPanel does not exist 
and " .drawString("",,) "  does not exist
*/
//110 points version: included creativity, multi-level ihneritance, included composion and included motions for waiting screen on phone
import javax.swing.*; //import graphical user interface (GUI)
import java.awt.*;
import java.util.Date; //import date
public class BobDinhQinwei
{
 Date now = new Date(); //create date and time
 private JFrame frame;
 private Phone phone;
 //first ball
 private int x = 6;
 private int y = 30;
 boolean up = false;
 boolean down = false;
 boolean left = false;
 boolean right = false;
 //second ball
 private int x2 = 280;
 private int y2 = 30;
 boolean up2 = false;
 boolean down2 = false;
 boolean left2 = false;
 boolean right2 = false;
 //main
public static void main(String[] args) 
 {
  new BobDinhQinwei().run(); //go to function run
 }
private void run() 
 {
  frame = new JFrame("iPhone"+now); //name of this program if executed ("iPhone") and also show time, date
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //terminate the program if close
  phone = new Phone();
  frame.getContentPane().add(BorderLayout.CENTER, phone); //add layout 
  frame.setVisible(true); // it is visible
  frame.setResizable(false); // dont allow resize phone
  frame.setSize(300, 600); //size of the window
  //goto moving function
  moving();
 }
//draw phone
class Phone extends JPanel 
{
  private static final long serialVersionUID = 1L; //fix compatible warning older version
  private Cam cam;
  private Home home;
  private Speaker sp;
  private App3 app3;
 public Phone()
  {
   //composition
   cam = new Cam(130,15,30,10);
   home = new Home(130,528,30,30); 
   sp=new Speaker(140,5,10,10);
   app3 = new App3(40, 100);
  }
 public void paint(Graphics g)
  {
   home.drawHome(g);
   sp.drawsp(g);
   cam.drawCam(g);
   g.setColor(Color.gray);
   g.fillRect(4, 30, this.getWidth()-7, this.getHeight()-70);
   app3.drawApp3(g);
   //draw 1st ball
   g.setColor(Color.black);
   g.fillOval(x, y, 10, 10);
   //draw 2nd ball         
   g.setColor(Color.black);
   g.fillOval(x2, y2, 10, 10);
   }
}
//draw applications
class App
{
  protected int xa1;
  protected int xa2;
  public App(int x1, int y1)
  {
    xa1 = x1;
    xa2 = y1;
  }
 public void drawApp(Graphics g)
  {
    g.setColor(Color.blue);
    g.fillRect(40,40,30,30);
    g.setColor(Color.black);
    g.drawString("Facebook",30,80);
    g.setColor(Color.red);
    g.drawString(""+now,60,500); //print out date anhd time (not update real-time, have to restart program for the time to continue update)
    g.setColor(Color.white);
    g.fillRect(50,45,5,20);
    g.fillRect(44,52,17,3);
    g.fillRect(50,45,10,2);
    g.setColor(Color.green);
  }
}
 
class App2 extends App
{
  protected int xap3;
  protected int yap3;
  public App2(int x2, int y2)
  {
    super(x2,y2);
    xap3=x2;
    yap3=y2;
  }
 public void drawApp2(Graphics g)
  {
    super.drawApp(g);
    g.fillRect(xap3+3,yap3,30,30);
    g.setColor(Color.black);
    g.drawString("Facetime",30,150);
    g.setColor(Color.white);
    g.fillRect(50,109,15,11);
    g.fillRect(45,112,3,5);
    g.fillRect(67,112,3,5);
    g.setColor(Color.green);
  }
}
// multi level inheritance
class App3 extends App2 
{
  protected int xap4;
  protected int yap4;
  public App3(int x3, int y3)
  {
    super(x3,y3);
    xap4=x3;
    yap4=y3;
  }
   public void drawApp3(Graphics g)
  {
    super.drawApp2(g);
    g.fillRect(xap4+90,yap4-62,30,30);
    g.setColor(Color.black);
    g.drawString("iMessages",120,80);
    g.setColor(Color.white);
    g.fillOval(133,42,24,20);
  }
}

//draw camera

class Cam
{
   private int xc;
   private int yc;
   private int xyc;
   private int yxc;
 public Cam(int xc, int yc, int xyc, int yxc)
   {
    this.xc = xc;
    this.yc = yc;
    this.xyc = xyc;
    this.yxc = yxc;
   }
 public void drawCam(Graphics g)
   {
    g.setColor(Color.black);
    g.fillOval(xc,yc,xyc,yxc);
    }
 }
//draw home button
class Home
 {
   private int xh;
   private int yh;
   private int xyh;
   private int yxh;
  public Home(int xh, int yh, int xyh, int yxh)
   {
     this.xh = xh;
     this.yh = yh;
     this.xyh = xyh;
     this.yxh = yxh;
    }
   public void drawHome(Graphics g)
    {
     g.drawOval(xh,yh,xyh,yxh);
     }
 }
//draw speaker
class Speaker
{
 private int xs;
 private int ys;
 private int xys;
 private int yxs;
public Speaker(int xs, int ys, int xys, int yxs)
 {
  this.xs = xs;
  this.ys = ys;
  this.xys = xys;
  this.yxs = yxs;
 }
public void drawsp(Graphics g)
 {
  g.setColor(Color.black);
  g.fillOval(xs,ys,xys,yxs); 
 }
}
// Moving function
private void moving() 
{
 // motion of both ball2 and ball1 (x2 and x, y2 and y, right, left,up,down, right2,left2,up2,down2)
while(true)
{
 if(x >= 280)
 {
  right = false;
  left = true;
 }
  if(x2 >= 280)
 {
  right2 = false;
  left2 = true;
  }
  if(x <= 7)
  {
   right = true;
   left = false;
  }
  if(x2 <= 7)
  {
   right2 = true;
   left2 = false;
  }
  if(y >= 510)
  {
    up = true;
    down = false;                
  }
  if(y2 >= 510)
  {
    up2 = true;
    down2 = false;
  }
  if(y <= 30)
  {
     up = false;
     down = true;
  }
  if(y2 <= 30)
  {
  up2 = false;
  down2 = true;
   }
  if(up)
   {
     y--;
   }
  if(up2)
   {
    y2--;
   }
  if(down)
   {
    y++;
   }
  if(down2)
   {
    y2++;
   }
  if(left)
  {
   x--;
  }
  if(left2)
  {
   x2--;
   }
  if(right)
  {
   x++;
  }
  if(right2)
  {
  x2++;
  }
  try
  {
  Thread.sleep(5); //set speed 
  }
  catch (Exception nothing)
  {
    nothing.printStackTrace();
  } // incase any exception happens but it will not happen so there is nothing in the curly braces, i also add just in case something happens, we will fix easier because it leaves trace
  frame.repaint(); //control, update paint cycle
  }
 }
}