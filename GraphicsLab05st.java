//////////////////////////////////////////////////////////////////////////////////////////
// 100 points (maybe extra credit) version                                              //
// This program demonstrates compound, nested, graphics, randomness                     //
// I only explain the first part of the program because the rest follows the same logic //
// Bob Dinh and Quinwwei                                                                //
//////////////////////////////////////////////////////////////////////////////////////////

/*    ▄▄▄▄    ▒█████   ▄▄▄▄       ▓█████▄  ██▓ ███▄    █  ██░ ██ 
  ▓█████▄ ▒██▒  ██▒▓█████▄   ▒██▀ ██▌▓██▒ ██ ▀█   █ ▓██░ ██▒
  ▒██▒ ▄██▒██░  ██▒▒██▒ ▄██  ░██   █▌▒██▒▓██  ▀█ ██▒▒██▀▀██░
   ▒██░█▀   ▒██   ██░▒██░█▀    ░▓█▄   ▌░██░▓██▒  ▐▌██▒░▓█ ░██ 
   ░▓█  ▀█▓░ ████▓▒░░▓█  ▀█▓ ░▒████▓ ░██░▒██░   ▓██░░▓█▒░██▓
  ░▒▓███▀▒░ ▒░▒░▒░ ░▒▓███▀▒  ▒▒▓  ▒ ░▓  ░ ▒░   ▒ ▒  ▒ ░░▒░▒
  ▒░▒   ░   ░ ▒ ▒░ ▒░▒   ░     ░ ▒  ▒  ▒ ░░ ░░   ░ ▒░ ▒ ░▒░ ░
  ░    ░ ░ ░ ░ ▒   ░    ░     ░ ░  ░  ▒ ░   ░   ░ ░  ░  ░░ ░
  ░          ░ ░   ░            ░     ░           ░  ░  ░  ░*/

/*________/\\\__________________________________________________________________        
 _____/\\\\/\\\\_______________________________________________________________       
  ___/\\\//\////\\\___/\\\_________________________________________________/\\\_      
   __/\\\______\//\\\_\///___/\\/\\\\\\____/\\____/\\___/\\_____/\\\\\\\\__\///__     
    _\//\\\______/\\\___/\\\_\/\\\////\\\__\/\\\__/\\\\_/\\\___/\\\/////\\\__/\\\_    
     __\///\\\\/\\\\/___\/\\\_\/\\\__\//\\\_\//\\\/\\\\\/\\\___/\\\\\\\\\\\__\/\\\_   
      ____\////\\\//_____\/\\\_\/\\\___\/\\\__\//\\\\\/\\\\\___\//\\///////___\/\\\_  
       _______\///\\\\\\__\/\\\_\/\\\___\/\\\___\//\\\\//\\\_____\//\\\\\\\\\\_\/\\\_ 
        _________\//////___\///__\///____\///_____\///__\///_______\//////////__\///__*/

import java.awt.*;
import java.applet.*;
import java.util.*;

public class GraphicsLab05st extends Applet
{
  private static final long serialVersionUID = 1309; //assign ID for the program
  public void paint(Graphics g)
  {
    Draw d = new Draw(); //create a new class
    d.draw4b(g); //goto draw4b
    d.random(g); //go to random
    g.setColor(Color.red);                                                                 
    g.drawString("Time execution: ",600,10);                                               // |
    g.drawString("Logs: ",980,70);                                                         // |---> write log words (title)
    g.drawString("Color, circles executions (listed by which finished first: ) ",9,9);     // |
  }
}
class Draw 
{ 
  public void draw4b(Graphics g)
 {
  //draw 4 squares (red, blue, green)
  g.setColor(Color.red);
  g.fillRect(10,100,30,30);
  g.setColor(Color.green);
  g.fillRect(10,250,30,30);
  g.setColor(Color.blue);
  g.fillRect(10,400,30,30);
 } 
  //declare value
  int r=0;
  int ge=0;
  int b=0;
  String logs=null;
  int y=0;
  int y4=70;
  int y2 =1000;
  int x10=700;
  int forl = 0;
public void random(Graphics g)
 {
  //create random number for colors
  g.setColor(Color.black);
  logs="Generating random color..."; //print out log
  g.drawString(""+logs,y2,y4+=15);  
  Random random = new Random();
  r = random.nextInt(256) + 0;
  ge = random.nextInt(256) + 0;
  b = random.nextInt(256) + 0;
  loop(g); //go to loop function
 }
  public void loop(Graphics g)
  {
    //finding max value
    int max = Math.max(r,ge);
     if (max >= b)
      {
     if (max == r) //nested if else
     {
          logs="Red has the highest value"; 
          g.drawString(""+logs,y2,y4+=15);  //print out log
          drawRed(g); //goto draw red function
        }
     else
     {
       logs="Green has the highest value";
        g.drawString(""+logs,y2,y4+=15); //print out log
        drawGreen(g); //goto draw green function
     }
      }
      else if (max <= b)
      {
          logs="Blue has the highest value";
          g.drawString(""+logs,y2,y4+=15); 
       drawBlue(g); //goto draw blue function
      }
      else 
      {
       loop(g);
      }
   }
  int count=0;
  int count2=0;
  int x=10;
  int x2=-40;
  int ic=1;
  int t3=0;
  int ano3=0;
  long startTime;
  public void drawRed(Graphics g)
  {
   startTime = System.nanoTime();
   ano3++; //prevent stackoverflow
   Color re = new Color(r, ge, b); //the choosen color
   if(count<17)
   {
      g.setColor(re); //set color
      g.fillOval(x+=50, 100, 30, 30); //draw
      count++;
      random(g); //go back to random function
      }
  if(count2<17)
  {
    if (count2==16 && t3==0) //compound
       {
        t3++;
        g.setColor(Color.black);
        long difference = System.nanoTime() - startTime;
        logs="Total execution time for red ";
        g.drawString(""+ logs + String.format("%d millisecond, %d microsecond ( %d nanosecond).", difference/1000000,difference/1000,difference),x10,y+=15);  
       }
      g.setColor(re);
      g.fillOval(x2+=50, 150, 30, 30);
      count2++;
      random(g);
  }
 
  if (count==17 && count2==17 && ic==1) //prevent stackoverflow, also compound
   {
    ic++;
    g.setColor(Color.black);
    g.drawString("Red done!",10,forl+=25); //print red done in the first place if red is done first if not, second, third place
   }
  if (ano3<36) //prevent stackoverflow
  {
  random(g); //goto random
  }
  }
  // the rest of the program follow the same logic
  int count3 = 0;
  int count4=0;
  int x3 =10;
  int x4=-40;
  int ib =0;
  int t2 =0;
  int ano2=0;
  long startTime2;
  public void drawGreen(Graphics g)
  {
   startTime2 = System.nanoTime();
   ano2++;
   Color re = new Color(r, ge, b);
   if(count3<17)
   {
     g.setColor(re);
     g.fillOval(x3+=50, 250, 30, 30);
     count3++;
     random(g);
   }
   if(count4<17)
   {
   if (count4==16 && t2==0)
       {
       t2++;
       g.setColor(Color.black);
       long difference = System.nanoTime() - startTime2;
       logs="Total execution time for green ";
       g.drawString(""+ logs + String.format("%d millisecond, %d microsecond ( %d nanosecond).", difference/1000000,difference/1000,difference),x10,y+=15); 
       }
  g.setColor(re);
     g.fillOval(x4+=50, 300, 30, 30);
     count4++;
     random(g);
   }
   if (count3==17 && count4==17 && ib==0)
   {
   ib++;
   g.setColor(Color.black);
   g.drawString("Green done!",10,forl+=25); 
   }
   if (ano2<36)
   {
   random(g);
   }
  }
  int count5=0;
  int count6=0;
  int x5 =10;
  int x6=-40;
  int id =0;
  int t =0;
  int ano =0;
  long startTime3;
  public void drawBlue(Graphics g) 
  {
     startTime3 = System.nanoTime();
     ano++;
     Color re = new Color(r, ge, b);
     if(count5<17)
    {
    g.setColor(re);
       g.fillOval(x5+=50, 400, 30, 30);
       count5++;
       random(g);
    }
    if(count6<17)
    {
    if (count6==16 && t==0)
        {
        t++;
        g.setColor(Color.black);
        long difference = System.nanoTime() - startTime3;
        logs="Total execution time for blue ";
        g.drawString(""+ logs + String.format("%d millisecond, %d microsecond ( %d nanosecond).", difference/1000000,difference/1000,difference),x10,y+=15); 
        }
   g.setColor(re);
      g.fillOval(x6+=50, 450, 30, 30);
      count6++;
      random(g);
    }
    if (count5==17 && count6==17 && id ==0)
    {
       id ++;
       g.setColor(Color.black);
    g.drawString("Blue done!",10,forl+=25); 
    }
    if (ano<36)
    {
     random(g);
    }
 }
}
  
