import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.*;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

 class AES
 {
public static void main(String[] args)
    {
     Main main = new Main();
     main.run();
   }
 }
    class Main
    {
    private static SecretKeySpec secretKey;
    private static byte[] key;
 
    public static void setKey(String myKey)
    {
        MessageDigest sha = null;
        try {
            key = myKey.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
 
    public static String encrypt(String strToEncrypt, String secret)
    {
        try
        {
            setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
        }
        catch (Exception e)
        {
            System.out.println("Error while encrypting: " + e.toString());
        }
        return null;
    }
 
    public static String decrypt(String strToDecrypt, String secret)
    {
        try
        {
            setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        }
        catch (Exception e)
        {
            System.out.println("Error while decrypting: " + e.toString());
        }
        return null;
    }
      public void run()
      {
      Scanner ca = new Scanner(System.in);
      System.out.println("What do you want to do? encrypt, type \"e\", decrypt, type \"d\", exit, type \"exit\" (e/d/exit)");
      String cho = ca.nextLine();
      String e = "e";
      String d = "d";
      String ex = "exit";
      if (cho.equals(d))
      {
     Scanner scann=new Scanner(System.in);
     System.out.println("Enter key: ");
     String bob = scann.nextLine();
     final String secretKey = bob;
     Scanner sc = new Scanner(System.in);
     System.out.println("Enter text here:");
     String originalString = sc.nextLine();
     String encryptedString=originalString;
     String decryptedString = Main.decrypt(encryptedString, secretKey) ;
     System.out.println(decryptedString);
      }
      else if(cho.equals(e)) {
      Scanner sc = new Scanner(System.in);
      System.out.println("Enter text here:");
      String originalString = sc.nextLine();
       Scanner scann=new Scanner(System.in);
      System.out.println("Enter key: ");
      String bob = scann.nextLine();
      final String secretKey = bob;
      String encryptedString = Main.encrypt(originalString, secretKey);
      System.out.println(encryptedString);
      }
      else if(cho.equals(ex))
      {
        System.exit(0);
      }
      else 
      {
        System.out.println("Type again!!");
       run(); 
      }
    }
   }

